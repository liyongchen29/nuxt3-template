import { SbtAuthWallet } from '@sbtauth/sbtauth-wallet'
import { ConnectedWallet, useWallet } from '../store/wallet'

const sbtauthPlugin = defineNuxtPlugin((nuxtApp) => {
  const { $config, $pinia } = nuxtApp
  const walletStore = useWallet($pinia)
  const sbtauth = new SbtAuthWallet({
    developMode: !$config.isProd,
    defaultChainId: $config.isProd ? '0x38' : '0x61',
    targetUrl: $config.isProd
      ? 'https://test-connect.sbtauth.io'
      : 'https://connect.sbtauth.io',
    uiConfig: {
      theme: 'light',
      locale: 'en-US',
      showWallets: true
    }
  })

  sbtauth.provider.on('accountsChanged', (data: string[] | undefined) => {
    if (data && data.length > 0) {
      const address = data[0]
      const wallet: ConnectedWallet = {
        label: 'sbtauth',
        chain: 'evm',
        icon: SbtAuthWallet.icon,
        chainId: sbtauth.provider.chainId,
        accounts: [{ address }],
        provider: sbtauth.provider
      }
      walletStore.addEvmWallet(Object.freeze(wallet))
    }
  })
  sbtauth.provider.on('disconnect', () => {
    walletStore.removeEvmWallet('sbtauth')
  })

  return {
    provide: {
      sbtauth
    }
  }
})

export default sbtauthPlugin

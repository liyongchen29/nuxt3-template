import { ofetch } from 'ofetch'
import { Yapi } from './yapi'
import { ApiProvider } from '~/types/api'

const api = defineNuxtPlugin((nuxtApp) => {
  const { $config } = nuxtApp
  const apiProvider: ApiProvider = {
    fetch: ofetch.create({
      baseURL: $config.public.baseUrl,
      headers: { 'Content-Type': 'application/json' },
      parseResponse: (data) => {
        const response = JSON.parse(data)
        if (response.code !== '000') {
          throw new Error(response._data.msg)
        } else {
          return response.data
        }
      }
    })
  }
  return {
    provide: {
      yapi: new Yapi(apiProvider)
    }
  }
})

export default api

import Onboard from '@web3-onboard/core'
import injectedModule from '@web3-onboard/injected-wallets'

const onboardPlugin = defineNuxtPlugin(() => {
  const MAINNET_RPC_URL = 'https://mainnet.infura.io/v3/<INFURA_KEY>'

  const injected = injectedModule()

  const onboard = Onboard({
    wallets: [injected],
    chains: [
      {
        id: '0x1',
        token: 'ETH',
        label: 'Ethereum Mainnet',
        rpcUrl: MAINNET_RPC_URL
      }
    ]
  })

  return { provide: { onboard } }
})

export default onboardPlugin

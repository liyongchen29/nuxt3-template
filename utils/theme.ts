import { GlobalThemeOverrides } from 'naive-ui'

export const themeOverrides: GlobalThemeOverrides = {
  common: {
    primaryColor: '#3366FF',
    primaryColorHover: '#3366ee',
    primaryColorPressed: '#3366ee'
  },
  Button: {
    textColor: '#000000'
  }
}
